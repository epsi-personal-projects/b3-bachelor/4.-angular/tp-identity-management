import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { LDAP_USERS } from '../model/ldap-mock-data';
import { UserLdap } from '../model/user-ldap';
import { UsersService } from '../service/users.service';

@Component({
  selector: 'app-ldap-list',
  templateUrl: './ldap-list.component.html',
  styleUrls: ['./ldap-list.component.scss']
})
export class LdapListComponent implements OnInit, AfterViewInit{
  displayedColumns: string[] = ['nomComplet', 'mail', 'employeNumero'];
  dataSource = new MatTableDataSource<UserLdap>([]);
  unactiveSelected = false;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(
    private usersService: UsersService,
    private router: Router
  ){}

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    this.getUsers();
  }

  ngAfterViewInit(): void {
    
  }

  // J'ai changé le filter afin de filtrer géneriquement la string, c'est pa pratique de filtrer la uniquement le startsWIth
  public filterPredicate(data: UserLdap, filter: string): boolean {
    return !filter || data.nomComplet.toLowerCase().includes(filter);
  }

  public applyFilter($event: KeyboardEvent): void {
    const FILTER_VALUE = ($event.target as HTMLInputElement).value;
    this.dataSource.filter = FILTER_VALUE.trim().toLowerCase();
  }

  public unactiveChanged($event: MatSlideToggleChange): void {
    this.unactiveSelected = $event.checked;
    this.getUsers();
  }

  private getUsers(): void {
    this.usersService.getUsers()
      .subscribe(users => {
        if(this.unactiveSelected) {
          this.dataSource.data = users.filter(user => user.active === false);
        } else {
          this.dataSource.data = users;
        }
      })
  }

  public edit(login: string) {
    this.router.navigate(['/user', login]).then(e => {
      if(!e) {
        console.log("Navigation has failed!");
      }
    })
  }
}
