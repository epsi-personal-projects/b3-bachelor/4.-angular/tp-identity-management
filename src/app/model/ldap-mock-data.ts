import {UserLdap} from './user-ldap';

export const LDAP_USERS: UserLdap[] = [
    {
        login: 'test.v1',
        nom: 'v1',
        prenom: 'test1',
        nomComplet: 'V1 test1',
        motDePasse: null,
        mail: 'test.v1@epsi.fr',
        role: 'ROLE_USER',
        employeNumero: 1234,
        employeNiveau: 120,
        dataEmbauche: '2020-01-01',
        publisherId: 1,
        active: false
    },
    {
        login: 'test.v2',
        nom: 'v2',
        prenom: 'test2',
        nomComplet: 'V2 test2',
        motDePasse: null,
        mail: 'test.v2@epsi.fr',
        role: 'ROLE_USER',
        employeNumero: 5678,
        employeNiveau: 210,
        dataEmbauche: '2020-02-02',
        publisherId: 2,
        active: true
    },
    {
        login: 'test.v3',
        nom: 'v3',
        prenom: 'test3',
        nomComplet: 'V3 test3',
        motDePasse: null,
        mail: 'test.v3@epsi.fr',
        role: 'ROLE_USER',
        employeNumero: 9012,
        employeNiveau: 310,
        dataEmbauche: '2020-03-03',
        publisherId: 3,
        active: true
    }
]