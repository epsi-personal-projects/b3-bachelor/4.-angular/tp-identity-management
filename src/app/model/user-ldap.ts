export interface UserLdap {
    login: string;
    nom: string;
    prenom: string;
    nomComplet: string;
    motDePasse: string;
    mail: string;
    role: string;
    employeNumero: number;
    employeNiveau: number;
    dataEmbauche: string;
    publisherId: number;
    active: boolean;
}