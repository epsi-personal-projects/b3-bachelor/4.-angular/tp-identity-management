import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserLdap } from '../model/user-ldap';
import { UsersService } from '../service/users.service';

@Component({
  selector: 'app-ldap-detail',
  templateUrl: './ldap-detail.component.html',
  styleUrls: ['./ldap-detail.component.scss']
})
export class LdapDetailComponent {
  user: UserLdap;
  processLoadRunning = false;
  processValidateRunning = false;

  userForm = this.fb.group({
    login: [''],
    nom: [''],
    prenom: [''],
    passwordGroup: this.fb.group({
      password: [''],
      confirmPassword: ['']
    }),
    mail: {value: '', disabled: true}
  })

  constructor(
    private usersService: UsersService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router
  ){}

  ngOnInit(): void {
    this.getUser();
  }

  private getUser(): void {
    const login = this.route.snapshot.paramMap.get('id');

    this.usersService.getUser(login).subscribe(user => {
      this.user = user;
      console.log("LdapDetail getUser =");
      console.log(user);
    })
  }

  private formGetValue(name: string): any {
    return this.userForm.get(name).value;
  }

  public goToLdap(): void {
    this.router.navigate(['/users/list']);
  }

  public onSubmitForm(): void {}

  public updateLogin(): void {
    this.userForm.get('login').setValue((this.formGetValue('prenom') + '-' + this.formGetValue('nom')).toLowerCase())
    this.updateMail();
  }

  public updateMail(): void {
    return this.userForm.get('mail').setValue(this.formGetValue('login').toLowerCase() + '@epsi.lan')
  }

  public isFormValid(): boolean { return false }

}
